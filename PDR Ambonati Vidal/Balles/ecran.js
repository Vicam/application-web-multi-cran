$(function() {
		Ecran.init();	
	}); 
		
var Ecran = {
	getWidth : function() {return $(document).width()-10; },
	getHeight : function() {return $(document).height()-10;},
	
	draw : function(w,h,info) { 
	  var canvas = document.getElementById("playground"); //On prend l'id du playground
	  var context = canvas.getContext("2d"); //leading to the creation of a CanvasRenderingContext2D object representing a two-dimensional rendering context.
	  context.beginPath(); //Création d'un rectangle bleu
	  context.strokeStyle="blue";   
	  context.lineWidth="1";
	  console.log(WURFL.complete_device_name);
	  if (info == "Apple iPhone 5S") { 
	  	var iphoneDiv = "<div id='iphoneDiv' class='ui-widget-content' style ='position:absolute;top:60px;left:260px;background:green;width:50px;height:79px;'> <p>iPhone</p> </div>";
	  	$(document.body).append(iphoneDiv);
	  	 $( "#iphoneDiv" ).draggable();
	  //context.rect(0.5*(400-0.1*0.22*w),0.5*(400-0.1*0.23*h),0.1*0.22*w,0.1*0.23*h);
	  context.rect(10,50,w*10/194,h*10/177.08);
	  context.translate(10+w*10/194,0);
	  }
	  else if(info == "Mozilla Firefox"){ 
	  	var ordiDiv = "<div id='ordiDiv' class='ui-widget-content' style ='top:60px;left:560px;background:yellow;width:295px;height:144px;'> <p>browser</p> </div>";
	  	$(document.body).append(ordiDiv);
	  	 //$( "#ordiDiv" ).draggable({ axis:'y'});
	  	 $( "#ordiDiv" ).draggable();
	  	 var ordi = window.document.getElementById('ordiDiv');
	  	 //var position = ordi.offsetHeight;
	  	 //console.log('position' + position);
	  //context.rect(0.5*(400-0.1*w),0.5*(400-0.1*h),0.1*w,0.1*h);
	   context.rect(10,50,w*10/45.97,h*10/45.63);
	   context.translate(10+w*10/45.97,0);
	  }else if(info == "Apple iPad"){
	  	var ipadDiv = "<div id='ipadDiv' class='ui-widget-content' style ='position:absolute;top:60px;left:360px;background:red;width:148px;height:179px;'> <p>iPad</p> </div>";
	  	$(document.body).append(ipadDiv);
	  	 $( "#ipadDiv" ).draggable();
	  	context.rect(10,50,w*10/65.54,h*10/59.34);
	  	context.translate(10+w*10/65.54,0);
	  }
	  context.stroke();

	},
	
	initSocket : function() { 
		Ecran.socket = io.connect('http://'+SERVER_IP+':'+WEBSOCKET_PORT);
		$(document).ready(function (){
			Ecran.socket.emit('ecran-in', 
			{
				'w' : Ecran.getWidth(),
				'h' : Ecran.getHeight(),
				'info' : WURFL.complete_device_name,
			});

			Ecran.socket.on('draw', function(data) { 
				Ecran.draw(data.w,data.h,data.info);
			});
            })
	},
	
	init : function() {
		console.log("Initialisation");
		console.log(WURFL);
		Ecran.initSocket();
	}
	
}
		