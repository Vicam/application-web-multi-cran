	$(function() {
		Balls.init();	
	}); 

	function Ball(x, y, dx, dy) {

		this.x = x;
		this.y = y;
		this.dx = dx;
		this.dy = dy;
		
		this.old_x = x;
		this.old_y = y;   

		var hue = [ 
             [255,0,0], 
             [255,255,0], 
             [0,255,0], 
             [0,255,255], 
             [0,0,255], 
             [255,0,255] 
         ]; 
         

		this.move = function() {
			this.old_x = this.x;
			this.old_y = this.y;		
			this.x += this.dx;
			this.y += this.dy;
		};
		
		//efface la balle
		this.clear = function(ctx) {
			ctx.clearRect(this.old_x-Balls.relativeRadius*1.5, this.old_y-Balls.relativeRadius*1.5, Balls.relativeRadius*3, Balls.relativeRadius*3);
		};
		
		// dessine la balle
		this.draw = function(ctx) {

			var grad;
             grad = ctx.createLinearGradient(0,0,1200,1200);
             for(var h = 0; h <hue.length; h++) 
             { 
                 var color = "rgb("+hue[h][0]+","+hue[h][1]+","+hue[h][2]+")"; 
                 grad.addColorStop(h/6,color); 
             } 


			ctx.beginPath();
			ctx.fillStyle = grad;
			ctx.arc(this.x, this.y, Balls.relativeRadius, 0, Math.PI * 2, true);
			ctx.closePath();
			ctx.fill();
		};
		
		this.checkBounds = function() {
			var leftX = this.x-Balls.relativeRadius;
			var rightX = this.x+Balls.relativeRadius;
			var topY = this.y-Balls.relativeRadius;
			var bottomY = this.y+Balls.relativeRadius;		
			
			if(leftX <= 0 && topY <=Balls.heightLeft) 
				return Balls.LEFT;
			if(leftX <= 0 && topY >=Balls.heightLeft) 
				return Balls.LEFTI;
			if(rightX >= Balls.getWidth() && topY <= Balls.heightRight)
				return Balls.RIGHT;
			if(rightX >= Balls.getWidth() && topY >=Balls.heightRight)
				return Balls.RIGHTI;
			if(topY <= 0) 
				return Balls.TOP;
			if(bottomY >= Balls.getHeight())
				return Balls.BOTTOM;
			

			return 0;
		};
		
		this.actBounds = function() {
			var bound = this.checkBounds();

		if(bound == Balls.TOP) {
			this.y = Balls.relativeRadius;
			this.dy = Math.abs(this.dy);
		} else if(bound == Balls.BOTTOM) {
			this.y = Balls.getHeight()-Balls.relativeRadius;
			this.dy = -Math.abs(this.dy);
		} else if(bound == Balls.RIGHTI) {
			this.x = Balls.getWidth()-Balls.relativeRadius;
			this.dx = -Math.abs(this.dx);
		} else if(bound == Balls.LEFTI){
			this.x = Balls.relativeRadius;
			this.dx = Math.abs(this.dx);
		}else if(bound != 0) {
			Balls.bounceBall(this, this.x, this.y, this.dx, this.dy, bound);				
		}
		
	}
	
}

var Balls = {
	TOP : 1,
	RIGHT: 2,
	BOTTOM: 3,
	LEFT : 4,	
	RIGHTI :5,	
	LEFTI : 6,

	//radius : 15,
	balls : [],
	


	init : function() {
		console.log("Initialisation");
		Balls.zone = $('#zone')[0];
		Balls.initSocket();
		Balls.updateViewport();
		Balls.initBallSpawn();
		window.setInterval(Balls.draw, 1000/50);
		$(window).resize(Balls.updateViewport);  

		console.log(WURFL.complete_device_name);
	},
	
	updateViewport : function() {
		console.log("MAJ dimension");
		var newWidth = $(document).width()-10;
		var newHeight = $(document).height()-10;
		
		var wHeight;
		var wWidth;
		if(WURFL.complete_device_name == "Apple iPhone 5S"){
				wHeight = newHeight/3.88;
				wWidth = newWidth/4.22;
			}else if(WURFL.complete_device_name == "Mozilla Firefox") {
				wHeight = newHeight;
				wWidth = newWidth;
			}else if(WURFL.complete_device_name == "Apple iPad"){
				wHeight = newHeight/1.438;
				wWidth = newWidth/1.436;
			}

		Balls.socket.emit('jeu-in', 
			{
				'w' : wWidth,
				'h' : wHeight,
			});
		var radius;
		//Balls.relativeRadius = Math.min(newWidth, newHeight)/100*Balls.radius;	
		if(WURFL.complete_device_name == "Apple iPhone 5S"){
				radius = 10*3.88;
			}else if(WURFL.complete_device_name == "Mozilla Firefox") {
				radius = 10;
			}else if(WURFL.complete_device_name == "Apple iPad"){
				radius = 10*1.438;
			}
		Balls.relativeRadius = radius;
		Balls.zone.width = newWidth;
		Balls.zone.height = newHeight;

		Balls.socket.on('jeu-out',function(data){
			if(WURFL.complete_device_name == "Apple iPhone 5S"){
				Balls.heightLeft = data.heightLeft*3.88;
				Balls.heightRight = data.heightRight*3.88;
			}else if(WURFL.complete_device_name == "Mozilla Firefox") {
				Balls.heightLeft = data.heightLeft;
				Balls.heightRight = data.heightRight;
			}else if(WURFL.complete_device_name == "Apple iPad"){
				Balls.heightLeft = data.heightLeft*1.438;
				Balls.heightRight = data.heightRight*1.438;
			}
			console.log('heightLeft' + Balls.heightLeft + 'heightRight' + Balls.heightRight);
		});

		
	},
	
	initSocket : function() {
		console.log('http://'+SERVER_IP+":"+WEBSOCKET_PORT);
		Balls.socket = io.connect('http://'+SERVER_IP+":"+WEBSOCKET_PORT);
		Balls.socket.on("ball-in", function(data) {
/*			var y;
			if(WURFL.complete_device_name  == "Apple iPhone 5S"){
				y = data.y*3.43;
			}else if(WURFL.complete_device_name  == "generic web browser") {
				y = data.y;
			}	*/
			Balls.spawnBall(
				(data.bound == Balls.LEFT) ? Balls.relativeRadius : Balls.getWidth()-Balls.relativeRadius,
				data.y,
				(data.bound == Balls.LEFT ? Math.abs(data.dx) : -Math.abs(data.dx)),
				data.dy
				);
		});
	},
	
	initBallSpawn : function() {
		$(Balls.zone).mousedown(function(downEvent) {
			$(Balls.zone).mouseup(function(upEvent){
				$(Balls.zone).unbind("mouseup");
				if(upEvent.clientX-downEvent.clientX==0 || upEvent.clientY-downEvent.clientY==0 ){ 
					var dx = 5;
					var dy = 5;
				}else{
				    var dx = (upEvent.clientX-downEvent.clientX)/Balls.getWidth()*25;  
				    var dy = (upEvent.clientY-downEvent.clientY)/Balls.getHeight()*25;  
				}
				Balls.spawnBall(upEvent.clientX, upEvent.clientY, dx, dy);
			})
		});
	},
	
	draw : function() {
		var ctx = Balls.zone.getContext("2d");
		ctx.clearRect(0, 0, Balls.getWidth(), Balls.getHeight());
		for(var ballIdx in Balls.balls) {
			Balls.balls[ballIdx].draw(ctx);   		
			Balls.balls[ballIdx].move();
			Balls.balls[ballIdx].actBounds();
		}
	},
	
	spawnBall : function(x, y, dx, dy)	 {
		console.log("Spawning Ball at "+x+"/"+y+" (Diff: "+dx+"/"+dy+")");
		Balls.balls.push(new Ball(x, y, dx, dy));		
	},
	
	bounceBall : function(ball, x, y, dx, dy, bound) {
		console.log("Removing Ball");
		var idx = Balls.balls.indexOf(ball);
		Balls.balls.splice(idx, 1);

/*		Balls.socket.on('type',function(data){
			//console.log('data.info' + data.info);
			if(data.info == "Apple iPhone 5S"){
				yc = y*3.43;
				x = x*4.22;
			}else if(data.info == "generic web browser") {
				yc = y;
				x = x;
				dy =dy;
				dx = dx;
			}	
		});*/
			console.log('y'+y+'dy'+dy+'dx'+dx+'bound'+bound);
			Balls.socket.emit("ball-out", {
			//'y' : y/Balls.getHeight(),
			'y'  : y,
			'dy' : dy,
			'dx' : dx,
			'bound' : bound,
			'info' :WURFL.complete_device_name,

			});
	},
	
	
/*	getWidth : function() {
		var width;
			if(WURFL.complete_device_name == "Apple iPhone 5S"){
				width = Balls.zone.width*4.22;
			}else if(WURFL.complete_device_name == "generic web browser") {
				width = Balls.zone.width;
			}
	 return width; },

	getHeight : function() {
		var height;
			if(WURFL.complete_device_name == "Apple iPhone 5"){
				height = Balls.zone.height*3.43;
			}else if(WURFL.complete_device_name == "generic web browser") {
				height = Balls.zone.height;
			}
	 return height; },*/

	getWidth : function() { return Balls.zone.width; },
	getHeight : function() { return Balls.zone.height; },
	
}
