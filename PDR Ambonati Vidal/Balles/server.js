require("./config.js");
var connect = require('connect');
var io = require("socket.io").listen(WEBSOCKET_PORT);

var clients = [];
var w = [];
var h = [];
var physicalH = [];
var physicalW = [];
var we = [];
var he = [];
var info =[];
var info1 =[];

var Balls = {
	TOP : 1,
	RIGHT: 2,
	BOTTOM: 3,
	LEFT : 4,
	RIGHTI :5,	
	LEFTI : 6,
};

io.sockets.on('connection', function (socket) {
		clients.push(socket);
	
		socket.on("ball-out", function(data) {
			var idx = clients.indexOf(socket);
			var targetIdx = idx;
		    info[idx] = data.info;
			var yc;
			var left = data.bound == Balls.LEFT;
			if(left && idx > 0) {
				targetIdx--;
				data.bound = Balls.RIGHT;

				if(info[idx] == "Mozilla Firefox" && info[idx-1] == "Apple iPhone 5S"){
					yc = data.y*3.88;
				}else if(info[idx] == "Apple iPhone 5S" && info[idx-1] == "Mozilla Firefox"){
					yc = data.y/3.88;
				}else if(info[idx] == "Mozilla Firefox" && info[idx-1] == "Apple iPad"){
					yc = data.y*1.438;
				}else if(info[idx] == "Apple iPad" && info[idx-1] == "Mozilla Firefox"){
					yc = data.y/1.438;
				}else if(info[idx] == "Apple iPhone 5S" && info[idx-1] == "Apple iPad"){
					yc = data.y/2.6;
				}else if(info[idx] == "Apple iPad" && info[idx-1] == "Apple iPhone 5S"){
					yc = data.y*2.6;
				}
				/*else if(info[idx] == "Apple iPad" && info[idx-1] == "Apple iPhone 5S"){
					yc = 
				}*/
				else {
					yc = data.y;
				}

			} else if(!left && idx < clients.length-1){
				targetIdx++;
				data.bound = Balls.LEFT;
				if(info[idx] == "Mozilla Firefox" && info[idx+1] == "Apple iPhone 5S"){
					yc = data.y*3.88;
				}else if(info[idx] == "Apple iPhone 5S" && info[idx+1] == "Mozilla Firefox"){
					yc = data.y/3.88;
				}else if(info[idx] == "Mozilla Firefox" && info[idx+1] == "Apple iPad"){
					yc = data.y*1.438;
				}else if(info[idx] == "Apple iPad" && info[idx+1] == "Mozilla Firefox"){
					yc = data.y/1.438;
				}else if(info[idx] == "Apple iPhone 5S" && info[idx+1] == "Apple iPad"){
					yc = data.y/2.6;
				}else if(info[idx] == "Apple iPad" && info[idx+1] == "Apple iPhone 5S"){
					yc = data.y*2.6;
				}
				else{
					yc = data.y;
				}
			} else {
				yc = data.y;
				// La balle rebondit normalement
			}						
			//console.log('yc'+yc+'dx'+data.dx+'dy'+data.dy+'bound'+data.bound+'idx'+idx);

			clients[targetIdx].emit("ball-in", {
				'y': yc,
				'dx': data.dx,
				'dy': data.dy,
				'bound': data.bound,
			//data
			});
		});
			
		socket.on("disconnect", function() {
			clients.splice(clients.indexOf(socket), 1);
			console.log('Ecran déconnecté');
			});
			
		socket.on('ecran-in', function (data) {
			var idx = clients.indexOf(socket);
			var targetIdx = idx;	
			we[idx] = data.w;
			he[idx] = data.h;
			console.log('Nouvel écran de dimension :' + data.w + ' x ' + data.h + ' info : ' + data.info + ' idx : ' + idx);
			info1[idx] = data.info;

			for(var i = 0; i <= idx; i++){
				clients[idx].emit("draw", {
					'w': we[i],
					'h': he[i],
					'info' : info1[i],
				});
			}

			});

		socket.on('jeu-in', function (data) {
			var idx = clients.indexOf(socket);
			var targetIdx = idx;			

			w[idx] = data.w;
			h[idx] = data.h;
			console.log('Nouvel écran de dimension :' + w[idx] + ' x ' + h[idx]);


			var heightLeft;
			var heightRight;
			
			if(idx!=0){
				heightLeft = Math.min(h[idx],h[idx-1]);
				console.log('heightLeft:' + heightLeft +'heightRight:' + heightRight + ' idx:'+ idx );
				

				if(idx == 1){

				clients[idx].emit('jeu-out',{
					'heightLeft':heightLeft,
					'heightRight':h[idx],
				});
				clients[idx-1].emit('jeu-out',{
					'heightRight':heightLeft,
					'heightLeft' :h[0],
				});
				}else{

				clients[idx].emit('jeu-out',{
					'heightLeft':heightLeft,
					'heightRight':h[idx],
				});

				clients[idx-1].emit('jeu-out',{
					'heightRight':heightLeft,
					'heightLeft' :Math.min(h[idx-1],h[idx-2]),
				});
				}
				
			}else{
				heightLeft = h[idx];
				console.log('heightLeft:' + heightLeft +'heightRight:' + heightRight + ' idx:'+ idx );
				clients[idx].emit('jeu-out',{
					'heightLeft':heightLeft,
					'heightRight':heightLeft,	
				});
	
			}
			
	});

	});
	
connect.createServer(
    connect.static(__dirname)
).listen(SERVER_PORT,SERVER_IP);
