require("./config.js");
var WebSocketServer = require('websocket').server; 

var http = require('http');
var path = require('path');
var fs = require('fs');

var server = http.createServer(function(request, response) {
  var content = '';
  var fileName = path.basename(request.url);
  var localFolder = __dirname+"/"; 

  if(fileName === 'client-side.html' || fileName === 'video.mp4' ){
    content = localFolder + fileName;
    
      fs.readFile(content,function(err,contents)
      {
        if(!err)
        {
          response.end(contents);
        } else {
          console.dir(err);
        };
      });

     
    } else {
        response.writeHead(404, {'Content-Type': 'text/html'});
        response.end('<h1>Sorry, the page you are looking for cannot be found.</h1>');
    };

});

server.listen(8000, function() {
    console.log((new Date()) + ' Server is listening on port 8000');
});

wsServer = new WebSocketServer({ 
    httpServer: server,
    autoAcceptConnections: false
});

function originIsAllowed(origin) {
  return true; 
}


var clients = []; 

wsServer.on('request', function(request) { 
    if (!originIsAllowed(request.origin)) {
      request.reject();
      console.log((new Date()) + ' Connection from origin ' + request.origin + ' rejected.');
      return;
    }

    var connection = request.accept('echo-protocol', request.origin); 
    clients.push(connection); //add client
    console.log((new Date()) + ' Connection accepted [' + clients.length + ']'); 
    
      var idx = clients.indexOf(connection);
      var targetIdx = idx; 
      
      
    for(var i in clients)
    {

      if (clients[i] == connection)
      {
        clients[i].sendUTF('You are connected to the server!');
        continue;
      }
      clients[i].sendUTF('We have a new client!'); 
    };


    connection.on('message', function(message) 
    {
      if (message.type === 'utf8') 
      {
        console.log('Received Message: ' + message.utf8Data);
        for(var i in clients) 
        {
          clients[i].sendUTF(message.utf8Data);
        };   
      }
      else if (message.type === 'binary') {
        console.log('Received Binary Message of ' + message.binaryData.length + ' bytes');
        connection.sendBytes(message.binaryData);
      }
    });

    connection.on('close', function(reasonCode, description) {
      var i = clients.indexOf(clients);
      clients.splice(i,1);
      console.log((new Date()) + ' Peer ' + connection.remoteAddress + ' disconnected.');
    });
});