video
=========

client-side --> Html5 WebSockets

server-side --> Node.js Server with WebSocket Module

======================================================================================================

Execute node server-side.js

Either double click on client-side.html or hit localhost:8000/client-side.html with a browser.

===================================================

It works for many sockets. Meaning many clients can connect to our server. Each client can play or pause the video for all the clients.
